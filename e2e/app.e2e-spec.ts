import { IntegrifyCommunityInternetPage } from './app.po';

describe('integrify-community-internet App', function() {
  let page: IntegrifyCommunityInternetPage;

  beforeEach(() => {
    page = new IntegrifyCommunityInternetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
