# Integrify Community Internet

Integrify.net (Because it's about bloody time Australians got good internet.) is a project that aims to facilitate communities to come together on a common issue, with the broader goal of launching successful community currencies.

The common issue Integrify aims to solve first is teaching communities how they can buy fast, reliable internet at wholesale prices and then distribute it out at cost price to the community.
This will be managed with transparent bank accounts using this website, and when the community has some programmers in that area which are component with managing this website, they will break off from the main website and run their own system 100%.




## Vision Stages
It is my vision there will be a bank account for each of the 2630 postcodes in Australia, which will manage community internet first, and then so much more.  Some communities in USA are starting to get their act together and have dropped the cost of their internet from $120 USD to $45 USD per month.

*If we really are going to create "The Internet of Things": Good, fast, reliable, transparent, cost price internet, is a must, or else it's all just a pipe dream.*



The internet is stage one, and the tool we use to unite each area.

**Stage 1:** Postcode area builds there list providing internet, cheaper and faster than Telstra.

**Stage 2:** Teach programmers how they can manage their own website in their area by introducing systems like https://coderdojo.com

**Stage 3:** Provide other services like a community owned Woolworths alternative, that is cheaper and 100% transparent on the finances.

**Stage 4:** Programmers in that area launch a community currency, and manage the currencies relationship it has with fiat currencies, and other cryptocurrencies.

**Stage 5:** it's up to you!



**POTENTIAL FUTURE COMMUNITY OWNED SERVICES:**

1.  Internet (first and foremost obviously)
2.  Community Owned Woolworths upgrade. (Includes farming, inventory systems created here will allow us to get to true cost economics.)
3.  Education (Arts, Programming, Science etc.)
4.  Housing
5.  Postal service
6.  Child Minding
7.  Global Village Construction Set https://vimeo.com/16106427
8.  Uber-like Transport system
9.  Community Medical Center
10. Community Solar Grids
11. Fuel Generation (use your imagination)
12. Decentralised Internet of things powered by projects like "Project Oaken" https://hack.ether.camp/public/project-oaken---hardware-oracle-prototype-by-ethembeddedcom
13. Community Banking



**Comparison Table - why someone would turn to Community Internet**

| Feature                         | Big Telco (i.e. Telstra/NBN)    | Community Owned |
| --------------------------------|---------------------------------|-----------------|
| Local Technical Support         |       NO                        | YES             |
| Live access to bank accounts    |       NO                        | YES             |
| Cost Price Internet             |       NO                        | YES             |
| Up to 110Mbs download & upload  |       NO                        | YES             |
| Unlimited Data                  |       NO                        | YES             |
| Pay for what you use            |       NO                        | YES             |
| Local Purposeful Jobs Created   |       NO                        | YES             |
| Half price internet             |       NO                        | YES             |





##Technology Stack
The technology use to make this all possible is outlined below.


### Transparent bank accounts
Transparent Bank Accounts will be created using uphold.com API (https://uphold.com/en/developer/api)
For an end user to have an experience of transferring AUD to an AUD bank account and seeing the funds in an uphold powered bank account the funds must be converted to BTC, this is becuase there is no company in Australia that is offering this service yet.
We will initially hold the funds in AUD for easy understanding, but eventually the funds will be held in another currency, however AUD conversion "observed" via AUD for simplicity for people who are not used to thinking in multiple currencies.


### Transfer of AUD to BTC to Uphold.com
The legal groundwork has been laid for bitcoin to be purchased automatically, via http://bitcointradingcompany.com.au/
Otherwise the cost price is 0.6% (trading fee)+ 0.5% (uphold.com fee) to convert it back to AUD after it has arrived in uphold.com, and a further 0.5% fee to BitcoinTradingCompany.com.au to provide this service, all funds bitcointradingcompany.com.au make will also be shown via transparent banking.

Total cost to load flexible bank accounts that powers transparency = 0.6% + 0.5% + 0.5% = 1.6% is a very fair fee to create transparent bank accounts, we aim to lower these costs once communities start launching their own currencies to trade in.
(if anyone has any other solutions to make this cheaper and possible we are all ears.)


### Modular Design
Angular2 Framework has been chosen for this open source project because as postcode communities develop they will want to break off and manage themselves, the Angular2 framework is perfect for this.


### Wireless Internet
Thanks to advancements in wireless internet, a network can be rolled out very quickly, using similar technology to the mobile phone network.


### Wholesale Internet Connection
Wholesale internet can be purchased from the Tier 1 network providers, they are very motivated by money to sell wholesale internet.  A backhaul connection to any postcode in Australia can be setup within 6 weeks.






#Crowd Funding Milestones
Funding is split up into 2 Milestones


##Milestone 1
Each area will crowd fund $1000 dollars for an experience wireless IT engineer to study your postcode area and produce a report that details:
-equipment costs to start stage one of the ISP
-labour costs
-wholesale connection cost
-ongoing wholesale purchase cost.

There is not going to a 100% signup rate of the ISP on day one, so the report will also provide an infrastructure upgrade plan as more and more people start signing up, and will provide and estimate on what these upgrade stages will cost.

e.g. on day 1 the ISP might only have 50 people signed up, so really infrastructure only needs to be funded to support 50+ subscribers distributed throughout the postcode area, with equipment upgrades as more subscribers signup.

This report will lead us to Milestone 2.


##Milestone 2
This is the second crowd fund campaign to start the ISP.

####ISP Setup (Internet Service Providers)
Funding ISP's will be the safest investment for anyone looking to make investments knowing they are not putting their funds at risk.

All initial funders will be entitled to receive back twice their initial fund amount.  Each month as subscript payments come in they can choose to receive payback straight away, or reinvest it with the company to allow for more expansion and payment of services.

#####Financial Scenario Month 1
e.g. 
Lets say initial funding cost $45,000

If John funds the ISP $1000 dollars, he will receive back $2000 dollars.  Therefore he is guaranteed a profit of $1000 dollars and can start taking is profit as soon as the first subscription comes in.

If on month 1, 50 subscriptions come in at $50 dollars each, therefore $50 x 50 subscriptions = $2,500 comes in on month 1.

Immediately 10% of this will go back to the initial funders.
10% = $250 dollars
John funded $1000/$45000 = 0.02222222222%
John will receive 0.02222222222 x $250 = $5.55 on Month 1.
John can either choose to take the money or he can choose to reinvest it back into the ISP meaning he would have invested $1005.55 which means he is entitled to $2011.10, giving him a profit of $1005.55.

#####Financial Scenario Month 4
Lets say on month 4 there are now 1000 subscribers all paying $50/month.
Total Revenue = $50,000
10% back to initial investors = 10%*$50,000 = $5,000
John funded $1000/$45000 = 0.02222222222%
John will receive 0.02222222222 x $5,000 = $111.11 on Month 4.

#####Financial Scenario Month 12
Lets say on month 12 there are now 7000 subscribers all paying $50/month.
Total Revenue = $350,000
10% back to initial investors = 10%*$350,000 = $35,000
John funded $1000/$45000 = 0.02222222222%
John will receive 0.02222222222 x $35,000 = $777.77 on Month 12.

The signup rate with this ISP will be much faster through word of mouth because so many people are invested and will be sharing the company therefore now money will be wasted on industrial marketing costs.

#Stage 1 Development Roadblocks
I'm just trying to pull together this: https://docs.google.com/drawings/d/13KNjgk2awBshC1xk9XoqZWyGpeIqW_apRT5rAHZrizw/edit


##Tasks left to complete stage 1:
* Create the components in the google drawing above. 
* Configure the router.
* Configure firebase authentication with firebase, and lock certain components.
* Configure firebase database with angular forms module/bootstrap.

Once these tasks are done, we can go live with the site and starting the crowdfunding, and at the same time move on with stage 2 development below.

#Stage 2 Development Roadblocks

##Problem 1: Component that does automatic trading on BTCMarkets.net

Money Flow:
* Person fills out form puts a record on firebase database saying how much AUD they want converted to Bitcoin.
* They are told to send x amount AUD to AUD Bank Account via the BSB System.
* Admin confirms money has been deposit which adds a record to the firebase database and manually sends funds to BTCMarkets.net
* optional. server side code picks up there is money in btcmarkets.net account and executes trade, and sends funds straight to uphold.com then converts to AUD, so the money can be shown in Problem 2/Component 2 below.

BTCMarkets NodeJS API: https://github.com/naddison36/btc-markets



##Problem 2: Component that shows the balance of bank accounts with Uphold.com
Just a simple component that uses the Uphold API to show specified uphold.com bank accounts for different purposes, using NodeJS framework.

Uphold.com API Documentation: https://uphold.com/en/developer/api




#About framework

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.24.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).