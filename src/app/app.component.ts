import { Component } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  constructor(private af: AngularFire){
    console.log(af);
  }

  OnInit(){

  }

  login(){
    this.af.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup
    }).then(authState => {
      console.log("AFTER LOGIN", authState);
    })
  }

  logout(){
    this.af.auth.logout(
  );
  console.log(("USER LOGGED OUT"))
  }
}
