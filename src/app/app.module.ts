import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import { NavComponent } from './nav/nav.component';
import { TimelineComponent } from './timeline/timeline.component';
import { FooterComponent } from './footer/footer.component';

export const firebaseConfig = {
    apiKey: "AIzaSyAOCxNpBl4uFsBCEE-gvR4Ehtt7Bz7GYNQ",
    authDomain: "integrify-debdd.firebaseapp.com",
    databaseURL: "https://integrify-debdd.firebaseio.com",
    storageBucket: "integrify-debdd.appspot.com",
    messagingSenderId: "677769752498"
};

@NgModule({
  declarations: [
    AppComponent,
    MailingListComponent,
    NavComponent,
    TimelineComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
